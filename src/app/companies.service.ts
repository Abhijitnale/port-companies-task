import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class CompaniesService {
    key = "a2c76b8e50dc1752e132121b0166ad40";
    searchURL = "https://financialmodelingprep.com/api/v3/search";
    incomeStatementURL = "https://financialmodelingprep.com/api/v3/financials/income-statement";
    profileURL = "https://financialmodelingprep.com/api/v3/company/profile";
    ratingURL = "https://financialmodelingprep.com/api/v3/company/rating";
  
    constructor(private http: HttpClient) {

    }

    getCompanies(params) {
        params['apikey'] = this.key;
        return this.http.get(this.searchURL, { params: params });
    }

    downloadStatement(name):any {
        let params = { datatype: 'csv' };
        params['apikey'] = this.key;
        return this.http.get(this.incomeStatementURL + '/' + name, { params: params });
    }

    getCompanyProfile(name) {
        let params = {};
        params['apikey'] = this.key;
        return this.http.get(this.profileURL + '/' + name, { params: params });
    }

    getRating(name) {
        let params = {};
        params['apikey'] = this.key;
        return this.http.get(this.ratingURL + '/' + name, { params: params });
    }
}