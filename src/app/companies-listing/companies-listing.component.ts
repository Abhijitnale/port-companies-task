import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../companies.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Parser, unwind } from 'json2csv';
import { FileSaver } from 'file-saver';

@Component({
  selector: 'app-companies-listing',
  templateUrl: './companies-listing.component.html',
  styleUrls: ['./companies-listing.component.css']
})
export class CompaniesListingComponent implements OnInit {
  companies: any = [];
  filterForm: FormGroup;

  constructor(private companiesService: CompaniesService, private fb: FormBuilder) {
    this.filterForm = this.fb.group({
      query: [],
      limit: [10],
      exchange: ['NASDAQ']
    })
  }

  ngOnInit() {
    this.getCompanies();
  }

  getCompanies() {
    let params = this.filterForm.value;
    this.companiesService.getCompanies(params).subscribe(res => {
      this.companies = res;
    },
      err => {

      })
  }

  downloadStatement(name) {
    this.companiesService.downloadStatement(name).subscribe(res => {
      let result: any = res['financialStatementList']
      let fields = Object.keys(result[0].financials[0]);
      let data = result[0].financials;
      const json2csvParser = new Parser({ fields });
      const csv = json2csvParser.parse(data);
      var a = document.createElement('a');
      var blob = new Blob([csv], {type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);
  
      a.href = url;
      a.download = "statement.csv";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    })
  }

}
