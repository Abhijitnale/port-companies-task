import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../companies.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Parser, unwind } from 'json2csv';
import { FileSaver } from 'file-saver';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {
  company;
  rating;
  filterForm: FormGroup;
  name;

  constructor(private companiesService: CompaniesService, private fb: FormBuilder) {
    this.filterForm = this.fb.group({
      query: [],
      limit: [],
      exchange: []
    })
  }

  ngOnInit() {
    // this.filterForm.controls['query'].valueChanges.pipe(
    //   debounceTime(400),
    // ).subscribe(val=>{
    //   this.getCompantProfile(val);
    // })
  }

  getCompanyProfile() {
    this.companiesService.getCompanyProfile(this.filterForm.controls['query'].value).subscribe(res => {
      this.company = res['profile'];
      this.name = res['symbol'];
    },
      err => {

      })

    this.companiesService.getRating(this.filterForm.controls['query'].value).subscribe(res => {
      this.rating = res['rating'];
    })
  }

  downloadStatement() {
    this.companiesService.downloadStatement(this.name).subscribe(res => {
      let result: any = res['financials']
      let fields = Object.keys(result[0]);
      let data = result;
      const json2csvParser = new Parser({ fields });
      const csv = json2csvParser.parse(data);
      var a = document.createElement('a');
      var blob = new Blob([csv], {type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);
  
      a.href = url;
      a.download = "statement.csv";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    })
  }

  getRatings() {
    let stars = [];
    if (this.rating && this.rating.score) {
      for (let i = 1; i <= this.rating.score; i++) {
        stars.push(i);
      }
    }
    return stars;
  }
}
