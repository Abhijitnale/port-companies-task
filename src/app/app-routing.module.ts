import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesListingComponent } from './companies-listing/companies-listing.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';

const routes: Routes = [
  {
    path:'', redirectTo:'companies', pathMatch:'full'
  },
  {
    path:'companies',
    component:CompaniesListingComponent
  },
  {
    path:'company-details',
    component:CompanyDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
